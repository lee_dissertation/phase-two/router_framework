# mqtt_broker
This project runs an MQTT broker, allowing for communication on that protocol.
## Installation
This application is managed by the project lerring-service and installed by setup. 
Use [docker-compose](https://docs.docker.com/compose/) to run the application manually. 
```bash 
$ docker-compose up --build -d
```
## Usage
To connect, user must subscribe with a name prefixed by lerring-
## Roadmap
.
