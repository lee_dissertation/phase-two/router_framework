import requests
from file_handler import *
from lib.jwt_helpers import *
import subprocess
from lib.lerring_conf import LerringConf

"""
Represents an endpoint in this system, the main role of this is to handle reloading hosts files and recieving the uuid
"""
class Api():

	uuid=LerringConf('/lerring/lerring.conf').getUuid()

	@staticmethod
	def reloadHosts():
		"""
		Tells the docker api to reload the dns server
		"""
		print("Reloading Hosts!")
		token=SelfSignedJwtHS256(Api.uuid).createToken(
			payload={'permissions':['apps']},
			headers={'typ':'JWT'}
		)
		requests.get(
			url="http://localhost:8080/apps/exec?name=router_framework&container=router-dns&command=pkill%20-SIGHUP%20dnsmasq",
			headers={'Authorization':token}
		)

"""
Represents the Lease endpoint
"""
class LeaseApi(Api):

	@staticmethod
	def getLeases():
		"""
		Reads the lease file and returns an api formatted version to the server
		"""
		return LeaseFileReader.readFile(
			'/var/lib/dnsmasq/leases/dnsmasq.leases',
			split=' '
		)

"""
Represents the Host endpoint
"""
class HostApi(Api):

	@staticmethod
	def getHosts(isFormatted=True):
		"""
		GET endpoint for hosts
		Params:
		    isFormatted - true if formatted for api, false if formatted as file
		Returns:
		    formatted / non formatted version of hosts file
		"""
		hostsAsFile=HostFileReader.readFile(
			file='/var/lib/dnsmasq/hosts/hosts.dnsmasq',
			split=' '
		)
		return hostsAsFile if not isFormatted else HostFileReader.asApiFormat(hostsAsFile)

	@staticmethod
	def putHosts(newHosts):
		"""
		PUT endpoint for hosts
		Params:
		    newHosts - list containing all new hosts to add
		Returns:
		    updated hosts
		"""
		#remove any existing hostnames if they already exist
		formatted=HostApi.removeHosts(newHosts)
		for newHost in newHosts:
			HostFileReader.addToApiFormat(formatted,newHost)
		HostFileReader.writeToFile(
			file='/var/lib/dnsmasq/hosts/hosts.dnsmasq',
			entries=HostFileReader.asFileFormat(formatted)
		)
		HostApi.reloadHosts()
		return formatted

	@staticmethod
	def removeHosts(hostsToRemove):
		"""
		REMOVE endpoint for hosts
		Params:
		    hostsToRemove - list of hosts to remove
		Returns:
		    updated hosts
		"""
		formatted=HostFileReader.asApiFormat(
			HostFileReader.readFile(
				file='/var/lib/dnsmasq/hosts/hosts.dnsmasq',
				split=' '
			)
		)
		formatted=HostFileReader.removeFromApiFormat(formatted,hostsToRemove)
		HostFileReader.writeToFile(
			file='/var/lib/dnsmasq/hosts/hosts.dnsmasq',
			entries=HostFileReader.asFileFormat(formatted)
		)
		HostApi.reloadHosts()
		return formatted
