from meinheld import server
import falcon
from falcon_cors import CORS
import json
from distutils import util
from dns_api import *

class EndPoint():

	@staticmethod
	def jsonResponse(message):
		return json.dumps(message,indent=4)+"\n"

"""
Handles accessing the lease file
"""
class Lease(EndPoint):

	def on_get(self,req,resp):
		"""
		Returns all leases read from the file
		"""
		resp.status=falcon.HTTP_200
		resp.body=EndPoint.jsonResponse(LeaseApi.getLeases())

"""
Handles accessing the host file
"""
class Host(EndPoint):

	def on_get(self,req,resp):
		"""
		GET request returns all hosts formatted for api or as file depending on request parameter
		"""
		resp.status=falcon.HTTP_200
		formatted=req.get_param('formatted')
		if formatted is None:
			formatted = True
		resp.body=EndPoint.jsonResponse(HostApi.getHosts(bool(util.strtobool(str(formatted)))))

	def on_put(self,req,resp):
		"""
		PUT request handling updating the hosts file
		"""
		#check if an array
		postedData=json.loads(req.stream.read())
		if isinstance(postedData, list):
			resp.status=falcon.HTTP_200
			resp.body=EndPoint.jsonResponse(HostApi.putHosts(postedData))
		else:
			body=EndPoint.jsonResponse(
				{
					'message':'Unable to process request!',
					'error':'expected json array'
				}
			)
			resp.status=falcon.HTTP_400
			resp.body=body

	def on_delete(self,req,resp):
		"""
		DELETE request handling removing entries from hosts file
		"""
		resp.status=falcon.HTTP_200
		hostnames=req.get_param_as_list('hostname',required=True)
		resp.body=EndPoint.jsonResponse(HostApi.removeHosts(hostnames))

class Status(EndPoint):

	def on_get(self,req,resp):
		"""
		GET used to check if the api is running
		"""
		resp.status=falcon.HTTP_200
		#ToDo check dns container is up
		resp.body=EndPoint.jsonResponse(
			{'status':'up'}
		)

cors = CORS(allow_all_origins=True)
app = falcon.API(middleware=[cors.middleware])
app.add_route(r'/leases', Lease())
app.add_route(r'/hosts', Host())
app.add_route(r'/',Status())
