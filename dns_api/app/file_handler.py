"""
General class to handle the reading of a file
"""
class FileReader():

	@classmethod
	def readFile(cls,file,split=' '):
		"""
		Reads a file and parses into an array of lines
		Params:
		    file - filename to read
		    split - delimiter to split on
		Returns:
		    array of lines in file
		"""
		response=[]
		with open(file) as file:
			for line in file.readlines():
				if not line.strip():
					continue
				if line.startswith('#'):
					continue
				splitLine=line.strip().split(split)
				response.append(cls.readLine(splitLine))
		return response

	@classmethod
	def writeToFile(cls,file,entries):
		"""
		Writes an array of entries to a file
		Params:
		    file - filename to write to
		    entries - array containing lines to write
		"""
		with open(file,'w') as file:
			for entry in entries:
				file.write(cls.writeEntry(entry))

	@staticmethod
	def readLine(splitLine):
		"""
		Reads a line from a file, expected to be overriden depending on how a line should be read
		Param:
		    splitline - line to be read
		Returns:
		    string containing parsed line
		"""
		return str(splitLine)

	@staticmethod
	def writeEntry(entry):
		"""
		Converts an entry into a line
		Param:
		    Entry - entry to be converted to a line
		Returns:
		    String representation of the entry with a new line appended
		"""
		return str(entry)+'\n'

"""
Implementation of filereader to handle the lease file type
"""
class LeaseFileReader(FileReader):

	@staticmethod
	def readLine(splitLine):
		"""
		Reads a line and parses into a Dict object
		Param:
		    splitline - array split by delimiter to be parsed
		Returns:
		    Dict object containing a representation of the line
		"""
		return {
			'exp':splitLine[0],
			'mac':splitLine[1],
			'ip':splitLine[2],
			'hostname':splitLine[3]
		}

"""
Implements filereader for hostfiles
"""
class HostFileReader(FileReader):

	@staticmethod
	def asApiFormat(hostsAsFile):
		"""
		Formats the hostfile into the API recognisable version
		Params:
		    list of lines from hostfile
		Returns:
		    formatted lines into objects
		"""
		formattedHosts={}
		for host in hostsAsFile:
			formattedHosts=HostFileReader.addToApiFormat(formattedHosts,host)
		return formattedHosts

	@staticmethod
	def addToApiFormat(formattedHosts,host):
		"""
		Adds a host to its api format following a functional style
		Param:
		    formattedHosts - hash of hosts as objects
		    host - host to parse
		Return:
		    updated formattedHosts with new host
		"""
		ip=host['ip']
		hostname=host['hostname']
		if ip not in formattedHosts:
			formattedHosts[ip]=[]
		if hostname not in formattedHosts[ip]:
			formattedHosts[ip].append(hostname)
		return formattedHosts

	@staticmethod
	def removeFromApiFormat(formattedHosts,hostsToRemove):
		"""
		Removes an entry from the formattedHosts
		Param:
		    formattedHosts - hash of hosts as objects
		    hostsToRemove - list of hostnames to be removed
		Return:
		    updated formattedHosts with removed hosts
		"""
		namesToRemove=[host['hostname'] for host in hostsToRemove]
		for ip,hostnames in formattedHosts.items():
			for hostname in hostnames:
				if hostname in namesToRemove:
					hostnames.remove(hostname)
		return formattedHosts

	@staticmethod
	def asFileFormat(formattedHosts):
		"""
		Formats the API version of the hostfile into the file version
		Params:
		    formatted hosts - objects representing a host
		Returns:
		    hosts formatted for file reading
		"""
		hostsAsFile=[]
		for ip,hostnames in formattedHosts.items():
			for hostname in hostnames:
				hostsAsFile.append({'ip':ip,'hostname':hostname})
		return hostsAsFile

	@staticmethod
	def readLine(splitLine):
		"""
		Reads a line by splitting it on ip and hostname
		Params:
		    splitline - array containing the line split by a specified delimiter
		Return:
		    Dict object containing ip and hostname
		"""
		return {
			'ip':splitLine[0],
			'hostname':splitLine[1]
		}

	@staticmethod
	def writeEntry(entry):
		"""
		Deserialize an entry into the file format
		"""
		return entry['ip']+' '+entry['hostname']+'\n'

