# dns
This project manages dns and dhcp using the application dnsmasq, this will provide hostnames 
and ip addresses to nodes.
## Installation
This application is managed by the project lerring-service and installed by setup. 
Use [docker-compose](https://docs.docker.com/compose/) to run the application manually. 
```bash 
$ docker-compose up --build -d 
```
## Usage
swagger will be included
## Roadmap
Provide an interface for updating records
